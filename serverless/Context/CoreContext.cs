﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace serverless.Context
{
    public class CoreContext : DynamoDBContext, ICoreContext
    {
        public readonly IAmazonDynamoDB dynamoDbClient;

        public CoreContext(IAmazonDynamoDB dynamoDbClient) 
            : base(dynamoDbClient, new DynamoDBContextConfig
            {
                TableNamePrefix = String.Format(@"{0}.{1}.",
                    Environment.GetEnvironmentVariable("RUNTIME_ENV"),
                    Environment.GetEnvironmentVariable("BRANCH_NAME"))
            })
        {
            this.dynamoDbClient = dynamoDbClient;
        }
    }
}
