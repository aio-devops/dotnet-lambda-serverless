﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace serverless.Context
{
    public interface ICoreContext : IDynamoDBContext, IDisposable
    {
    }
}
