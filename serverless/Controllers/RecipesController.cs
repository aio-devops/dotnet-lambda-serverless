﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using serverless.Context;
using serverless.Models;

namespace serverless.Controllers
{
    [Route("{branch}/api/[controller]")]
    [ApiController]
    public class RecipesController : ControllerBase
    {
        private readonly ICoreContext _context;

        public RecipesController(ICoreContext coreContext)
        {
            _context = coreContext;
        }

        [HttpPost]
        public async Task Post([FromBody]Recipe recipe)
        {
            await _context.SaveAsync(recipe);
        }


        [HttpGet]
        public async Task<Recipe> Get()
        {
            return await _context.LoadAsync<Recipe>("1");
        }

        [HttpGet("{id}")]
        public async Task<Recipe> Get(string id)
        {
            return await _context.LoadAsync<Recipe>(id);
        }

    }
}