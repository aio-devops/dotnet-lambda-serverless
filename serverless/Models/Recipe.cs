﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace serverless.Models
{
    [DynamoDBTable("recipe")]
    public class Recipe 
    {
        public Recipe()
        {
            
        }

        [DynamoDBHashKey]
        public string Id { get; set; }

        public string Title { get; set; }

        public List<string> Instructions { get; set; }

        public string Author { get; set; }

        public List<string> Images { get; set; }
    }
}
