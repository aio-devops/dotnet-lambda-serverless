# Dotnet CI/CD Pipline for Serverless Microservices

### Project Setup

1. Clone the repository
	* `git clone https://gitlab.com/aio-devops/dotnet-lambda-serverless.git <FolderName>`
2. Follow the instructions for creating your AWS Credentials
    * [Creating AWS Keys](https://serverless.com/framework/docs/providers/aws/guide/credentials#creating-aws-access-keys) 
3. Add the following CI/CD variables to your gitlab project.
    * `AWS__AccessKey` created from step 2
    * `AWS__SecretKey` created from step 3
    * `AWS__REGION` choose your AWS region
4. (Opt) Continue for adding custom role to your services, currently only required for DynamoDB
    * Create a custom Role in AWS IAM, and attach permissions for DynamoDB, or attach DynamoDB Policies.
    * Add the role ARN to CI/CD variables as `AWS_ServlessServericeRole`

5. Now, push your first commit to master and your pipeline will fail at the `deploy` stage. That is because you need to deploy your infrastructure first, which is normally a one time thing. Currently it's a manual script that you can manually run on the `build` stage, called `deploy_infrastructure`, which will setup the AWS Gateway for your services to consume. (Only available in `master` branch!).
    > - Note: If you attempt to `drop_infrastructure` while there are active services, it will fail, because resources depend on that infrastructure. 

### Cleanup
* I've added a script to Clean Up your service/s if you want to tear it down (`sls remove`), you can go to GitLab's Operation>Environments and select `Cleanup` from the dopdown for that given environment.
    > - Note: This also requires your CI/CD variables to be configured properly, from step 3 in [Project Setup](Project-Setup)